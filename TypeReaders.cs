#region File Description
//-----------------------------------------------------------------------------
// TypeReaders.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#endregion

namespace SkinnedModel
{
    /// <summary>
    /// Loads SkinningData objects from compiled XNB format.
    /// </summary>
    public class SkinningDataReader : ContentTypeReader<SkinningData>
    {
        protected override SkinningData Read(ContentReader input,
                                             SkinningData existingInstance)
        {
            Dictionary<string, AnimationClip> animationClips;
            List<Matrix> bindPose, inverseBindPose;
            List<int> skeletonHierarchy;

            animationClips = input.ReadObject<Dictionary<string, AnimationClip>>();
            bindPose = input.ReadObject<List<Matrix>>();
            inverseBindPose = input.ReadObject<List<Matrix>>();
            skeletonHierarchy = input.ReadObject<List<int>>();

            return new SkinningData(animationClips, bindPose,
                                    inverseBindPose, skeletonHierarchy);
        }
    }


    /// <summary>
    /// Loads AnimationClip objects from compiled XNB format.
    /// </summary>
    public class AnimationClipReader : ContentTypeReader<AnimationClip>
    {
        protected override AnimationClip Read(ContentReader input,
                                              AnimationClip existingInstance)
        {
            TimeSpan duration = input.ReadObject<TimeSpan>();
            List<Keyframe> keyframes = input.ReadObject < List<Keyframe>>();

            return new AnimationClip(duration, keyframes);
        }
    }


    /// <summary>
    /// Loads Keyframe objects from compiled XNB format.
    /// </summary>
    public class KeyframeReader : ContentTypeReader<Keyframe>
    {
        protected override Keyframe Read(ContentReader input,
                                         Keyframe existingInstance)
        {
            int bone = input.ReadObject<int>();
            TimeSpan time = input.ReadObject<TimeSpan>();
            Matrix transform = input.ReadObject<Matrix>();

            return new Keyframe(bone, time, transform);
        }
    }
}
